#include <iostream>
#include "complex.h"

int main()
{
    {
        std::cout << "Default constructor ";
        Complex a;

        if (a.Real() == 0 && a.Imaginary() == 0)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }
 
    {
        std::cout << "Two parameters constructor (real and imaginary) ";
        Complex a (3, 4);
        if (a.Real() == 3 && a.Imaginary() == 4)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }

    {
        std::cout << "Constructor from real number ";
        Complex a(3);
        
        if (a.Real() == 3 && a.Imaginary() == 0)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }

    return 0;
}
