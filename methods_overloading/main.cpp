#include <iostream>
#include "complex.h"

int main()
{
    {
        std::cout << "Accumulate two reals to default constructed complex ";
        Complex a;
        double real = 2, imag = 3;

        a.accumulate(real, imag);

        if (a.Real() == 2 && a.Imaginary() == 3)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }
 
    {
        std::cout << "Accumulate one real ";
        Complex a;
        double real = 2, imag = 3;
        a.accumulate(real, imag);
        double realPart = 7;

        a.accumulate(realPart);

        if (a.Real() == 9 && a.Imaginary() == 3)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }

    {
        std::cout << "Accumulate complex number ";
        Complex a;
        double real = 2, imag = 3;
        a.accumulate(real, imag);

        a.accumulate(a);

        if (a.Real() == 4 && a.Imaginary() == 6)
            std::cout << "passed";
        else
            std::cout << "failed";
        std::cout << std::endl;
    }

    return 0;
}
