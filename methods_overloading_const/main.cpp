#include <iostream>
#include "complex.h"


void printComplex(std::ostream & out, Complex & c)
{
    out << c.Real() << " + i * (" << c.Imaginary() << ")";
}

int main()
{
    Complex a(2, -3);
    printComplex(std::cout, a);

    const Complex b(3, 5);
    printComplex(b);

    return 0;
}
